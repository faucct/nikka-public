use core::{
    convert::TryInto,
    fmt,
    mem,
    sync::atomic::{AtomicUsize, Ordering},
};

use lazy_static::lazy_static;
use spin::{Mutex, MutexGuard};
use volatile::Volatile;
use x86_64::{
    instructions::{interrupts, tables},
    registers::control::Cr2,
    structures::{
        idt::{Entry, EntryOptions},
        DescriptorTablePointer,
    },
    VirtAddr,
};

use ku::{
    memory::PageFaultInfo,
    process::{Info, MiniContext, TrapInfo},
};

use crate::{
    error::Result,
    log,
    log::{debug, error, info, trace, warn},
    memory::{mmu::PageTableFlags, size, Virt, DOUBLE_FAULT_IST_INDEX, PAGE_FAULT_IST_INDEX},
    process::{ModeContext, Pid, Process, Table},
    smp::{Cpu, LocalApic},
    time::{pit8254, rtc},
};


pub const DIVIDE_ERROR: usize = 0x00;
pub const DEBUG: usize = 0x01;
pub const NON_MASKABLE_INTERRUPT: usize = 0x02;
pub const BREAKPOINT: usize = 0x03;
pub const OVERFLOW: usize = 0x04;
pub const BOUND_RANGE_EXCEEDED: usize = 0x05;
pub const INVALID_OPCODE: usize = 0x06;
pub const DEVICE_NOT_AVAILABLE: usize = 0x07;
pub const DOUBLE_FAULT: usize = 0x08;
pub const INVALID_TSS: usize = 0x0A;
pub const SEGMENT_NOT_PRESENT: usize = 0x0B;
pub const STACK_SEGMENT_FAULT: usize = 0x0C;
pub const GENERAL_PROTECTION_FAULT: usize = 0x0D;
pub const PAGE_FAULT: usize = 0x0E;
pub const X87_FLOATING_POINT: usize = 0x10;
pub const ALIGNMENT_CHECK: usize = 0x11;
pub const MACHINE_CHECK: usize = 0x12;
pub const SIMD_FLOATING_POINT: usize = 0x13;
pub const VIRTUALIZATION: usize = 0x14;
pub const SECURITY_EXCEPTION: usize = 0x1E;


const EXCEPTION_COUNT: u8 = 0x20;

const PIC_FIRST_INTERRUPT: u8 = EXCEPTION_COUNT;

const PIC_BASE: usize = PIC_FIRST_INTERRUPT as usize;
pub const TIMER: usize = PIC_BASE;
pub const KEYBOARD: usize = PIC_BASE + 0x1;
pub const CASCADE: usize = PIC_BASE + 0x2;
pub const COM2: usize = PIC_BASE + 0x3;
pub const COM1: usize = PIC_BASE + 0x4;
pub const LPT2: usize = PIC_BASE + 0x5;
pub const FLOPPY_DISK: usize = PIC_BASE + 0x6;
pub const LPT1: usize = PIC_BASE + 0x7;

/// Номер обработчика прерываний [часов реального времени (Real-time clock, RTC)](https://en.wikipedia.org/wiki/Real-time_clock).
pub const RTC: usize = PIC_BASE + 0x8;

pub const FREE_29: usize = PIC_BASE + 0x9;
pub const FREE_2A: usize = PIC_BASE + 0xA;
pub const FREE_2B: usize = PIC_BASE + 0xB;
pub const PS2_MOUSE: usize = PIC_BASE + 0xC;
pub const COPROCESSOR: usize = PIC_BASE + 0xD;
pub const ATA0: usize = PIC_BASE + 0xE;
pub const ATA1: usize = PIC_BASE + 0xF;

const APIC_BASE: usize = ATA1 + 1;
pub const APIC_TIMER: usize = APIC_BASE;
pub const APIC_SPURIOUS: usize = APIC_BASE + 0x1;

const COUNT: usize = APIC_SPURIOUS + 1;


/// Информация о прерывании с некоторым номером.
pub struct Statistics {
    /// Сколько раз сработало это прерывание.
    count: AtomicUsize,

    /// Короткая мнемоника прерывания.
    mnemonic: &'static str,

    /// Имя прерывания.
    name: &'static str,
}


impl Statistics {
    const fn new(name: &'static str, mnemonic: &'static str) -> Statistics {
        Statistics {
            name,
            mnemonic,
            count: AtomicUsize::new(0),
        }
    }


    pub fn count(&self) -> usize {
        self.count.load(Ordering::Relaxed)
    }


    pub fn mnemonic(&self) -> &'static str {
        self.mnemonic
    }


    pub fn name(&self) -> &'static str {
        self.name
    }


    fn inc(&self) {
        self.count.fetch_add(1, Ordering::Relaxed);
    }
}


type InterruptStats = [Statistics; COUNT];


pub static INTERRUPT_STATS: InterruptStats = [
    Statistics::new("Divide Error", "#DE"),
    Statistics::new("Debug", "#DB"),
    Statistics::new("Non-maskable Interrupt", "#NM"),
    Statistics::new("Breakpoint", "#BP"),
    Statistics::new("Overflow", "#OF"),
    Statistics::new("Bound Range Exceeded", "#BR"),
    Statistics::new("Invalid Opcode", "#UD"),
    Statistics::new("Device Not Available", "#NA"),
    Statistics::new("Double Fault", "#DF"),
    Statistics::new("Coprocessor Segment Overrun", "#CS"),
    Statistics::new("Invalid TSS", "#TS"),
    Statistics::new("Segment Not Present", "#NP"),
    Statistics::new("Stack-Segment Fault", "#SS"),
    Statistics::new("General Protection Fault", "#GP"),
    Statistics::new("Page Fault", "#PF"),
    Statistics::new("Reserved 0x0F", "#0F"),
    Statistics::new("x87 Floating-Point Exception", "#MF"),
    Statistics::new("Alignment Check", "#AC"),
    Statistics::new("Machine Check", "#MC"),
    Statistics::new("SIMD Floating-Point Exception", "#XF"),
    Statistics::new("Virtualization Exception", "#VE"),
    Statistics::new("Reserved 0x15", "#15"),
    Statistics::new("Reserved 0x16", "#16"),
    Statistics::new("Reserved 0x17", "#17"),
    Statistics::new("Reserved 0x18", "#18"),
    Statistics::new("Reserved 0x19", "#19"),
    Statistics::new("Reserved 0x1A", "#1A"),
    Statistics::new("Reserved 0x1B", "#1B"),
    Statistics::new("Reserved 0x1C", "#1C"),
    Statistics::new("Reserved 0x1D", "#1D"),
    Statistics::new("Security Exception", "#SX"),
    Statistics::new("Reserved 0x1F", "#1F"),
    Statistics::new("Timer", "#TI"),
    Statistics::new("Keyboard", "#KB"),
    Statistics::new("Cascade", "#CA"),
    Statistics::new("COM2", "#C2"),
    Statistics::new("COM1", "#C1"),
    Statistics::new("LPT2", "#L2"),
    Statistics::new("Floppy Disk", "#FD"),
    Statistics::new("LPT1", "#L1"),
    Statistics::new("RTC", "#RT"),
    Statistics::new("Free 0x29", "#29"),
    Statistics::new("Free 0x2A", "#2A"),
    Statistics::new("Free 0x2B", "#2B"),
    Statistics::new("PS2 Mouse", "#MS"),
    Statistics::new("Coprocessor", "#CP"),
    Statistics::new("Primary ATA Hard Disk", "#PD"),
    Statistics::new("Secondary ATA Hard Disk", "#SD"),
    Statistics::new("APIC timer", "#AT"),
    Statistics::new("APIC spurious", "#AS"),
];


/// Инициализирует таблицу обработчиков прерываний [`static@IDT`].
pub(super) fn init() {
    unsafe {
        pic8259::init(PIC_FIRST_INTERRUPT);
    }

    IDT.load();

    interrupts::enable();

    info!("interrupts init");
}


/// Таблица обработчиков прерываний.
pub(crate) struct Idt([IdtEntry; COUNT]);


impl Idt {
    fn new() -> Self {
        let mut idt = [IdtEntry::missing(); COUNT];

        unsafe {
            idt[DOUBLE_FAULT]
                .set_handler_with_error(double_fault)
                .set_stack_index(DOUBLE_FAULT_IST_INDEX);

            idt[PAGE_FAULT]
                .set_handler_with_error(page_fault)
                .set_stack_index(PAGE_FAULT_IST_INDEX);
        }

        idt[DIVIDE_ERROR].set_handler(divide_error);
        idt[DEBUG].set_handler(debug);
        idt[NON_MASKABLE_INTERRUPT].set_handler(non_maskable_interrupt);
        idt[BREAKPOINT].set_handler(breakpoint);
        idt[OVERFLOW].set_handler(overflow);
        idt[BOUND_RANGE_EXCEEDED].set_handler(bound_range_exceeded);
        idt[INVALID_OPCODE].set_handler(invalid_opcode);
        idt[DEVICE_NOT_AVAILABLE].set_handler(device_not_available);
        idt[INVALID_TSS].set_handler_with_error(invalid_tss);
        idt[SEGMENT_NOT_PRESENT].set_handler_with_error(segment_not_present);
        idt[STACK_SEGMENT_FAULT].set_handler_with_error(stack_segment_fault);
        idt[GENERAL_PROTECTION_FAULT].set_handler_with_error(general_protection_fault);
        idt[X87_FLOATING_POINT].set_handler(x87_floating_point);
        idt[ALIGNMENT_CHECK].set_handler_with_error(alignment_check);
        idt[MACHINE_CHECK].set_handler(machine_check);
        idt[SIMD_FLOATING_POINT].set_handler(simd_floating_point);
        idt[VIRTUALIZATION].set_handler(virtualization);
        idt[SECURITY_EXCEPTION].set_handler_with_error(security_exception);

        idt[TIMER].set_handler(timer);
        idt[KEYBOARD].set_handler(keyboard);
        idt[CASCADE].set_handler(cascade);
        idt[COM2].set_handler(com2);
        idt[COM1].set_handler(com1);
        idt[LPT2].set_handler(lpt2);
        idt[FLOPPY_DISK].set_handler(floppy_disk);
        idt[LPT1].set_handler(lpt1);
        idt[RTC].set_handler(rtc);
        idt[FREE_29].set_handler(free_29);
        idt[FREE_2A].set_handler(free_2a);
        idt[FREE_2B].set_handler(free_2b);
        idt[PS2_MOUSE].set_handler(ps2_mouse);
        idt[COPROCESSOR].set_handler(coprocessor);
        idt[ATA0].set_handler(ata0);
        idt[ATA1].set_handler(ata1);
        idt[APIC_TIMER].set_handler(apic_timer);
        idt[APIC_SPURIOUS].set_handler(apic_spurious);

        Self(idt)
    }


    pub(crate) fn load(&self) {
        unsafe {
            let pseudo_descriptor = DescriptorTablePointer {
                base: Virt::from_ref(&self.0).into(),
                limit: (mem::size_of_val(&self.0) - 1).try_into().expect("the IDT is too large"),
            };
            tables::lidt(&pseudo_descriptor);
        }
    }
}


lazy_static! {
    /// Таблица обработчиков прерываний.
    pub(crate) static ref IDT: Idt = Idt::new();
}


#[repr(transparent)]
pub struct InterruptContext(ModeContext);


impl InterruptContext {
    pub fn is_user_mode(&self) -> bool {
        self.0.is_user_mode()
    }


    pub fn get(&self) -> ModeContext {
        self.0
    }


    pub fn set(&mut self, context: ModeContext) {
        Volatile::new(&mut self.0).write(context);
    }
}


impl fmt::Debug for InterruptContext {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{:?}", self.get())
    }
}


impl fmt::Display for InterruptContext {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", self.get())
    }
}


type InterruptHandler = fn(InterruptContext);


#[derive(Clone, Copy)]
struct IdtEntry(Entry<InterruptHandler>);


impl IdtEntry {
    const fn missing() -> Self {
        Self(Entry::missing())
    }


    fn set_handler(
        &mut self,
        handler: extern "x86-interrupt" fn(InterruptContext),
    ) -> &mut EntryOptions {
        unsafe { self.0.set_handler_addr(VirtAddr::from_ptr(handler as *const ())) }
    }


    fn set_handler_with_error(
        &mut self,
        handler: extern "x86-interrupt" fn(InterruptContext, u64),
    ) -> &mut EntryOptions {
        unsafe { self.0.set_handler_addr(VirtAddr::from_ptr(handler as *const ())) }
    }
}


#[derive(PartialEq)]
enum Fatal {
    Yes,
    No,
}


fn generic_trap(context: &mut InterruptContext, number: usize, info: Info, fatal: Fatal) {
    debug!(trap = INTERRUPT_STATS[number].name, %context, %info);

    INTERRUPT_STATS[number].inc();

    if context.is_user_mode() {
        let pid = Cpu::current_process().expect("user mode without a process");
        if pid == Pid::Current {
            context.set(ModeContext::kernel_context(Virt::default()));
            return;
        }
        let mut process =
            Table::get(pid).expect("failed to find the current process in the process table");

        if let Ok(log) = process.log() {
            log::user_events(pid, log);
            trace!(read_stats = ?log.read_stats(), write_stats = ?log.write_stats());
        } else {
            warn!(%pid, "log is not mapped properly");
        }

        // TODO: your code here.

        info!(
            trap = INTERRUPT_STATS[number].name,
            number,
            %info,
            %context,
            %pid,
            "user mode trap",
        );

        if fatal == Fatal::Yes {
            Table::free(process);
            Process::leave_user_mode();
        }
    } else {
        error!(
            trap = INTERRUPT_STATS[number].name,
            number,
            %info,
            %context,
            "kernel mode trap",
        );

        if fatal == Fatal::Yes {
            panic!(
                "kernel mode trap #{:?} - {}, context: {}",
                number, INTERRUPT_STATS[number].name, context,
            );
        }
    }
}


fn user_trap_handler(
    process: &mut MutexGuard<Process>,
    original_context: &mut InterruptContext,
    number: usize,
    info: Info,
) -> Result<()> {
    // TODO: your code here.
    unimplemented!();
}


extern "x86-interrupt" fn divide_error(mut context: InterruptContext) {
    generic_trap(&mut context, DIVIDE_ERROR, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn debug(mut context: InterruptContext) {
    generic_trap(&mut context, DEBUG, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn non_maskable_interrupt(mut context: InterruptContext) {
    generic_trap(&mut context, NON_MASKABLE_INTERRUPT, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn breakpoint(mut context: InterruptContext) {
    generic_trap(&mut context, BREAKPOINT, Info::None, Fatal::No);
}


extern "x86-interrupt" fn overflow(mut context: InterruptContext) {
    generic_trap(&mut context, OVERFLOW, Info::None, Fatal::No);
}


extern "x86-interrupt" fn bound_range_exceeded(mut context: InterruptContext) {
    generic_trap(&mut context, BOUND_RANGE_EXCEEDED, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn invalid_opcode(mut context: InterruptContext) {
    generic_trap(&mut context, INVALID_OPCODE, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn device_not_available(mut context: InterruptContext) {
    generic_trap(&mut context, DEVICE_NOT_AVAILABLE, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn double_fault(mut context: InterruptContext, error_code: u64) {
    lazy_static! {
        static ref STOP_ALL_CPUS: Mutex<()> = Mutex::new(());
    }

    let _ = STOP_ALL_CPUS.lock();

    generic_trap(
        &mut context,
        DOUBLE_FAULT,
        Info::Code(error_code),
        Fatal::Yes,
    );
    unreachable!();
}


extern "x86-interrupt" fn invalid_tss(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        INVALID_TSS,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


extern "x86-interrupt" fn segment_not_present(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        SEGMENT_NOT_PRESENT,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


extern "x86-interrupt" fn stack_segment_fault(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        STACK_SEGMENT_FAULT,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


extern "x86-interrupt" fn general_protection_fault(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        GENERAL_PROTECTION_FAULT,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


extern "x86-interrupt" fn page_fault(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        PAGE_FAULT,
        Info::PageFault {
            code: PageFaultInfo::from_bits_truncate(size::into_usize(error_code)),
            address: Cr2::read().into(),
        },
        Fatal::Yes,
    );
}


extern "x86-interrupt" fn x87_floating_point(mut context: InterruptContext) {
    generic_trap(&mut context, X87_FLOATING_POINT, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn alignment_check(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        ALIGNMENT_CHECK,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


extern "x86-interrupt" fn machine_check(mut context: InterruptContext) {
    generic_trap(&mut context, MACHINE_CHECK, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn simd_floating_point(mut context: InterruptContext) {
    generic_trap(&mut context, SIMD_FLOATING_POINT, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn virtualization(mut context: InterruptContext) {
    generic_trap(&mut context, VIRTUALIZATION, Info::None, Fatal::Yes);
}


extern "x86-interrupt" fn security_exception(mut context: InterruptContext, error_code: u64) {
    generic_trap(
        &mut context,
        SECURITY_EXCEPTION,
        Info::Code(error_code),
        Fatal::Yes,
    );
}


fn generic_pic_interrupt(number: usize) {
    INTERRUPT_STATS[number].inc();
    unsafe {
        pic8259::end_of_interrupt(number - PIC_BASE);
    }
}


extern "x86-interrupt" fn timer(_context: InterruptContext) {
    pit8254::interrupt();
    generic_pic_interrupt(TIMER);
}


extern "x86-interrupt" fn keyboard(_context: InterruptContext) {
    generic_pic_interrupt(KEYBOARD);
}


extern "x86-interrupt" fn cascade(_context: InterruptContext) {
    generic_pic_interrupt(CASCADE);
}


extern "x86-interrupt" fn com2(_context: InterruptContext) {
    generic_pic_interrupt(COM2);
}


extern "x86-interrupt" fn com1(_context: InterruptContext) {
    generic_pic_interrupt(COM1);
}


extern "x86-interrupt" fn lpt2(_context: InterruptContext) {
    generic_pic_interrupt(LPT2);
}


extern "x86-interrupt" fn floppy_disk(_context: InterruptContext) {
    generic_pic_interrupt(FLOPPY_DISK);
}


extern "x86-interrupt" fn lpt1(_context: InterruptContext) {
    generic_pic_interrupt(LPT1);
}


/// Обработчик прерываний [часов реального времени (Real-time clock, RTC)](https://en.wikipedia.org/wiki/Real-time_clock).
extern "x86-interrupt" fn rtc(_context: InterruptContext) {
    rtc::interrupt();
    generic_pic_interrupt(RTC);
}


extern "x86-interrupt" fn free_29(_context: InterruptContext) {
    generic_pic_interrupt(FREE_29);
}


extern "x86-interrupt" fn free_2a(_context: InterruptContext) {
    generic_pic_interrupt(FREE_2A);
}


extern "x86-interrupt" fn free_2b(_context: InterruptContext) {
    generic_pic_interrupt(FREE_2B);
}


extern "x86-interrupt" fn ps2_mouse(_context: InterruptContext) {
    generic_pic_interrupt(PS2_MOUSE);
}


extern "x86-interrupt" fn coprocessor(_context: InterruptContext) {
    generic_pic_interrupt(COPROCESSOR);
}


extern "x86-interrupt" fn ata0(_context: InterruptContext) {
    generic_pic_interrupt(ATA0);
}


extern "x86-interrupt" fn ata1(_context: InterruptContext) {
    generic_pic_interrupt(ATA1);
}


#[allow(unused_mut)] // TODO: remove before flight.
extern "x86-interrupt" fn apic_timer(mut context: InterruptContext) {
    INTERRUPT_STATS[APIC_TIMER].inc();

    // TODO: your code here.

    LocalApic::end_of_interrupt();
}


extern "x86-interrupt" fn apic_spurious(_context: InterruptContext) {
    INTERRUPT_STATS[APIC_SPURIOUS].inc();
    LocalApic::end_of_interrupt();
}


pub mod test_scaffolding {
    use super::{Idt, IdtEntry, InterruptContext, COUNT, DEBUG};


    static mut IDT: Idt = Idt([IdtEntry::missing(); COUNT]);


    pub fn set_debug_handler(handler: extern "x86-interrupt" fn(InterruptContext)) {
        unsafe {
            IDT = Idt::new();
            IDT.0[DEBUG].set_handler(handler);
            IDT.load();
        }
    }
}
