use alloc::vec::Vec;
use core::{fmt, mem};

use lazy_static::lazy_static;
use spin::{Mutex, MutexGuard};

use crate::{
    error::{
        Error::{NoProcess, NoProcessSlot},
        Result,
    },
    log::info,
    time,
};

use super::{Pid, Process};


#[allow(clippy::large_enum_variant)]
enum Slot {
    Free {
        pid: Pid,
        next: Option<Pid>,
    },
    Occupied {
        process: Mutex<Process>,
    },
}


impl fmt::Display for Slot {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Slot::Free { pid, next } => {
                write!(formatter, "Free {{ pid: {}, next: {:?} }}", pid, next)
            },
            Slot::Occupied { process } => write!(formatter, "Process {}", *process.lock()),
        }
    }
}


#[derive(Default)]
pub struct Table {
    free: Option<Pid>,
    process_count: usize,
    table: Vec<Slot>,
}


impl Table {
    pub(super) fn new(len: usize) -> Self {
        // TODO: your code here.
        unimplemented!();
    }


    #[allow(unused_mut)] // TODO: remove before flight.
    pub(super) fn allocate(mut process: Process) -> Result<Pid> {
        // TODO: your code here.
        unimplemented!();
    }


    pub fn free(process: MutexGuard<Process>) {
        // TODO: your code here.
    }


    pub fn get(pid: Pid) -> Result<MutexGuard<'static, Process>> {
        // TODO: your code here.
        test_scaffolding::process() // TODO: remove before flight.
    }
}


impl Drop for Table {
    fn drop(&mut self) {
        assert!(
            self.table.is_empty(),
            "should not drop non-empty process table"
        );
    }
}


unsafe fn forge_static_lifetime<T>(x: &T) -> &'static T {
    &*(x as *const T) as &'static T
}


lazy_static! {
    pub(super) static ref TABLE: Mutex<Table> = Mutex::new(Table::default());
}


#[doc(hidden)]
pub mod test_scaffolding {
    use alloc::vec;

    use lazy_static::lazy_static;
    use spin::{Mutex, MutexGuard};

    use crate::error::{Error::NoProcess, Result};

    use super::{forge_static_lifetime, Pid, Process, Slot, Table, TABLE};


    pub(super) fn process() -> Result<MutexGuard<'static, Process>> {
        assert!(TABLE.lock().table.is_empty());

        DUMMY_TABLE
            .lock()
            .as_ref()
            .map(|process| unsafe { forge_static_lifetime(process).lock() })
            .ok_or(NoProcess)
    }


    pub fn set_process(mut process: Process) {
        assert!(TABLE.lock().table.is_empty());

        process.set_pid(Pid::new(0));
        *DUMMY_TABLE.lock() = Some(Mutex::new(process));
    }


    lazy_static! {
        pub(super) static ref DUMMY_TABLE: Mutex<Option<Mutex<Process>>> = Mutex::new(None);
    }
}
