use core::convert::TryInto;
use core::fmt;

use crate::smp::Cpu;


#[derive(Clone, Copy, Eq, PartialEq)]
pub enum Pid {
    Current,
    Id {
        slot: u16,
        epoch: u32,
    },
}


impl Pid {
    pub(super) const MAX_COUNT: usize = (1 << Self::EPOCH_SHIFT);

    const EPOCH_SHIFT: u32 = u16::BITS;


    pub(super) fn new(slot: usize) -> Self {
        Self::Id {
            slot: slot.try_into().unwrap(),
            epoch: 0,
        }
    }


    pub(super) fn slot(&self) -> usize {
        let pid = match self {
            Self::Current => {
                Cpu::current_process()
                    .expect("no current process")
            },
            pid => {
                *pid
            },
        };

        if let Self::Id { slot, epoch: _ } = pid {
            slot.into()
        } else {
            panic!("wrong pid {:?} encountered when processig pid {:?}", pid, self)
        }
    }


    pub(super) fn next_epoch(&mut self) -> Self {
        if let Self::Id { slot: _, epoch } = self {
            *epoch += 1
        } else {
            panic!("can not increment epoch in Pid::Current when processing pid {:?}", self)
        }

        *self
    }
}


impl fmt::Debug for Pid {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        (self as &dyn fmt::Display).fmt(formatter)
    }
}


impl fmt::Display for Pid {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Current => { write!(formatter, "<current>") },
            Self::Id { slot, epoch } => { write!(formatter, "{}:{}", slot, epoch) },
        }
    }
}
