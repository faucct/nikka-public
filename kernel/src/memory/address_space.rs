use core::{any, fmt, mem, mem::MaybeUninit, ptr};

use lazy_static::lazy_static;
use spin::Mutex;

use crate::{
    error::{
        Error::{PermissionDenied, WrongAlignment},
        Result,
    },
    log::{info, trace, warn},
    process::Pid,
};

use super::{
    block::Block,
    frage::{Frame, Page},
    mapping::Mapping,
    mmu,
    mmu::{PageTableEntry, PageTableFlags},
    page_allocator::PageAllocator,
    Virt,
    FRAME_ALLOCATOR,
};

// Used in docs.
#[allow(unused)]
use crate::error::Error;


/// Структура для работы с виртуальным адресным пространством.
#[derive(Debug, Default)]
pub struct AddressSpace {
    /// Тип виртуального адресного пространства для логирования:
    /// - [`Kind::Invalid`] --- некорректное;
    /// - [`Kind::Base`] --- базовое;
    /// - [`Kind::Process`] --- относящееся к некоторому процессу.
    kind: Kind,

    /// Структура для управления отображением виртуальных страниц в физические фреймы.
    mapping: Mapping,

    /// Аллокатор виртуальных страниц.
    page_allocator: PageAllocator,

    /// Процесс, к которому относится данное адресное пространство.
    /// Содержит [`None`], если:
    /// - у процесса ещё нет [`Pid`] или
    /// - это адресное пространство не относится ни к какому процессу.
    pid: Option<Pid>,
}


impl AddressSpace {
    /// Инициализирует виртуальное адресное пространство текущим страничным отображением.
    pub(super) fn new(phys2virt: Page) -> Self {
        let mapping = Mapping::new(Mapping::current_page_directory(), phys2virt);
        let page_allocator =
            PageAllocator::new(unsafe { mapping.page_table_ref(mapping.page_directory()) });

        let address_space = Self {
            kind: Kind::Base,
            mapping,
            page_allocator,
            pid: None,
        };

        info!(%address_space, "init");

        address_space
    }


    /// Создаёт копию адресного пространства с копией страничного отображения.
    /// См. [`Mapping::duplicate()`].
    pub(crate) fn duplicate(&self) -> Result<Self> {
        let mapping = self.mapping.duplicate()?;

        let page_allocator =
            PageAllocator::new(unsafe { mapping.page_table_ref(mapping.page_directory()) });

        let address_space = Self {
            kind: Kind::Process,
            mapping,
            page_allocator,
            pid: None,
        };

        info!(%address_space, "duplicate");

        Ok(address_space)
    }


    /// Возвращает страничное отображение данного виртуального адресного пространства.
    pub fn mapping(&mut self) -> &mut Mapping {
        &mut self.mapping
    }


    /// Возвращает физический фрейм, в котором хранится корневой узел
    /// страничного отображения данного виртуального адресного пространства.
    pub fn page_directory(&self) -> Frame {
        self.mapping.page_directory()
    }


    /// Сохраняет информацию об идентификаторе процесса,
    /// который владеет этим адресным пространством.
    pub(crate) fn set_pid(&mut self, pid: Pid) {
        self.pid = Some(pid)
    }


    /// Переключает процессор в это виртуальное адресное пространство.
    pub(crate) fn switch_to(&self) {
        info!(address_space = %self, "switch to");

        unsafe {
            assert!(self.page_directory() != Frame::default());
            mmu::write_cr3(self.page_directory());
        }
    }


    /// Выделяет блок подряд идущих виртуальных страниц, достаточный для хранения объекта
    /// размером `size` **байт**.
    /// Ни выделения физической памяти, ни создания отображения станиц, не происходит.
    ///
    /// Если выделить заданный размер виртуальной памяти не удалось,
    /// возвращает ошибку [`Error::NoPage`].
    pub fn allocate(&mut self, size: usize) -> Result<Block<Page>> {
        self.page_allocator.allocate(size)
    }


    /// Отображает заданную виртуальную страницу `page` на заданный физический фрейм `frame`
    /// с указанными флагами доступа `flags`.
    ///
    /// Если `page` уже была отображена, то старое отображение удаляется,
    /// если только при этом не произойдёт замена прав доступа к странице с
    /// "только для ядра" на "доступно в пользовательском пространстве" ---
    /// [`PageTableFlags::USER_ACCESSIBLE`].
    /// В случае попытки такой замены отображение не меняется,
    /// а возвращается ошибка [`Error::PermissionDenied`].
    /// Если же отображение `page` поменялось, старый физический фрейм освобождается,
    /// если на него не осталось других ссылок.
    ///
    /// # Safety
    ///
    /// Вызывающий код должен гарантировать, что инварианты управления памятью в Rust'е
    /// не будут нарушены.
    /// В частности, не осталось ссылок, которые ведут в страницу `page`.
    pub unsafe fn map_page_to_frame(
        &mut self,
        page: Page,
        frame: Frame,
        flags: PageTableFlags,
    ) -> Result<()> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Аллоцирует физический фрейм и отображает на него заданную виртуальную страницу `page`
    /// с указанными флагами доступа `flags`.
    /// Подробнее см. [`AddressSpace::map_page_to_frame()`].
    ///
    /// # Safety
    ///
    /// Вызывающий код должен гарантировать, что инварианты управления памятью в Rust'е
    /// не будут нарушены.
    /// В частности, не осталось ссылок, которые ведут в страницу `page`.
    pub(crate) unsafe fn map_page(&mut self, page: Page, flags: PageTableFlags) -> Result<Frame> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Удаляет отображение заданной виртуальной страницы `page`.
    /// Физический фрейм освобождается, если на него не осталось других ссылок.
    ///
    /// # Safety
    ///
    /// Вызывающий код должен гарантировать, что инварианты управления памятью в Rust'е
    /// не будут нарушены.
    /// В частности, не осталось ссылок, которые ведут в удаляемую страницу.
    #[allow(unused)] // TODO: remove before flight.
    pub unsafe fn unmap_page(&mut self, page: Page) -> Result<()> {
        // TODO: your code here.
        Ok(()) // TODO: remove before flight.
    }


    /// Вспомогательный метод
    /// для [`AddressSpace::map_page_to_frame()`] и [`AddressSpace::unmap_page()`].
    /// Удаляет отображение заданной виртуальной страницы `page`, отображённой через запись `pte`.
    /// Физический фрейм освобождается, если на него не осталось других ссылок.
    ///
    /// # Safety
    ///
    /// Вызывающий код должен гарантировать, что инварианты управления памятью в Rust'е
    /// не будут нарушены.
    /// В частности, не осталось ссылок, которые ведут в удаляемую страницу.
    unsafe fn unmap_pte(page: Page, pte: &mut PageTableEntry) -> Result<()> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Аллоцирует нужное количество физических фреймов
    /// и отображает в них заданный блок виртуальных страниц `pages`
    /// с заданными флагами доступа `flags`.
    ///
    /// # Safety
    ///
    /// Вызывающий код должен гарантировать, что инварианты управления памятью в Rust'е
    /// не будут нарушены.
    /// В частности, не осталось ссылок, которые ведут в `pages`.
    pub unsafe fn map_block(&mut self, pages: Block<Page>, flags: PageTableFlags) -> Result<()> {
        for page in pages {
            self.map_page(page, flags)?;
        }

        Ok(())
    }


    /// Удаляет отображение заданного блока виртуальных страниц `pages`.
    /// Физические фреймы, на которые не осталось других ссылок, освобождаются.
    ///
    /// # Safety
    ///
    /// Вызывающий код должен гарантировать, что инварианты управления памятью в Rust'е
    /// не будут нарушены.
    /// В частности, не осталось ссылок, которые ведут в `pages`.
    pub unsafe fn unmap_block(&mut self, pages: Block<Page>) -> Result<()> {
        for page in pages {
            self.unmap_page(page)?;
        }

        Ok(())
    }


    /// Аллоцирует нужное количество физических фреймов
    /// и отображает в них срез элементов типа `T` заданного размера `len`
    /// с заданными флагами доступа `flags`.
    ///
    /// Возвращает срез в виде неинициализированной памяти.
    fn map_slice_uninit<T>(
        &mut self,
        len: usize,
        flags: PageTableFlags,
    ) -> Result<&'static mut [MaybeUninit<T>]> {
        if Page::SIZE % mem::align_of::<T>() != 0 {
            warn!(
                alignment = mem::align_of::<T>(),
                page_size = Page::SIZE,
                "can not handle alignments greater than the page size"
            );
            return Err(WrongAlignment);
        }

        let block = self.page_allocator.allocate(len * mem::size_of::<T>())?;

        unsafe {
            self.map_block(block, flags)?;
        }

        let slice = unsafe { block.try_into_mut_slice()? };

        trace!(
            %block,
            page_count = block.count(),
            slice = format_args!("[{}; {}]", any::type_name::<T>(), slice.len()),
            "mapped a slice",
        );

        Ok(slice)
    }


    /// Аллоцирует нужное количество физических фреймов
    /// и отображает в них срез элементов типа `T` заданного размера `len`
    /// с заданными флагами доступа `flags`.
    ///
    /// Инициализирует все элементы среза лямбдой `default`.
    pub fn map_slice<T, F: Fn() -> T>(
        &mut self,
        len: usize,
        flags: PageTableFlags,
        default: F,
    ) -> Result<&'static mut [T]> {
        let slice = self.map_slice_uninit(len, flags)?;

        for element in slice.iter_mut() {
            element.write(default());
        }

        Ok(unsafe { MaybeUninit::slice_assume_init_mut(slice) })
    }


    /// Аллоцирует нужное количество физических фреймов
    /// и отображает в них срез элементов типа `T` заданного размера `len`
    /// с заданными флагами доступа `flags`.
    ///
    /// Инициализирует память нулями.
    ///
    /// # Safety
    ///
    /// Нулевой битовый паттерн должен быть валидным значением типа `T`,
    /// см. [`MaybeUninit::zeroed()`].
    pub unsafe fn map_slice_zeroed<T>(
        &mut self,
        len: usize,
        flags: PageTableFlags,
    ) -> Result<&'static mut [T]> {
        let slice = self.map_slice_uninit(len, flags)?;

        for element in slice.iter_mut() {
            *element = MaybeUninit::zeroed();
        }

        Ok(MaybeUninit::slice_assume_init_mut(slice))
    }


    /// Удаляет отображение заданного среза `slice`.
    /// Физические фреймы, на которые не осталось других ссылок, освобождаются.
    ///
    /// # Safety
    ///
    /// - Срез должен был быть ранее выделен одним из методов `AddressSpace::map_slice*()`.
    /// - Также вызывающий код должен гарантировать, что инварианты управления памятью в Rust'е
    /// не будут нарушены.
    /// В частности, не осталось ссылок, которые ведут в `slice`.
    pub unsafe fn unmap_slice<T>(&mut self, slice: &mut [T]) -> Result<()> {
        let ptr_range = slice.as_ptr_range();
        let start = Page::new(Virt::from_ptr(ptr_range.start))?;
        let end = Page::new(Virt::from_ptr(ptr_range.end))?;

        let block = Block::new(start, end)?;
        let page_count = block.count();

        for element in slice.iter_mut() {
            ptr::drop_in_place(element as *mut T);
        }

        self.unmap_block(block)?;

        trace!(
            addr = %start.address(),
            page_count,
            slice = format_args!("[{}; {}]", any::type_name::<T>(), slice.len()),
            "unmapped a slice",
        );

        Ok(())
    }


    /// Аллоцирует нужное количество физических фреймов
    /// и отображает в них один элемент типа `T`
    /// с заданными флагами доступа `flags`.
    ///
    /// Инициализирует элемент лямбдой `default`.
    pub fn map_one<T, F: FnOnce() -> T>(
        &mut self,
        flags: PageTableFlags,
        default: F,
    ) -> Result<&'static mut T> {
        let slice = self.map_slice_uninit(1, flags)?;
        slice[0].write(default());
        Ok(unsafe { MaybeUninit::assume_init_mut(&mut slice[0]) })
    }


    /// Удаляет отображение заданного элемента `element`.
    /// Физические фреймы, на которые не осталось других ссылок, освобождаются.
    ///
    /// # Safety
    ///
    /// - Элемент должен был быть ранее выделен методом [`AddressSpace::map_one()`].
    /// - Также вызывающий код должен гарантировать, что инварианты управления памятью в Rust'е
    /// не будут нарушены.
    /// В частности, не осталось ссылок, которые ведут в `element`.
    pub unsafe fn unmap_one<T>(&mut self, element: &'static mut T) -> Result<()> {
        self.unmap_slice(Block::<Virt>::from_ref(element).try_into_mut_slice::<T>()?)
    }


    /// Проверяет доступность блока виртуальной памяти `block` на чтение и
    /// соответствие заданным флагам доступа `flags`.
    /// Возвращает иммутабельный срез типа `T`, расположенный в блоке `block`.
    ///
    /// Это позволяет объединить в одно действие проверку доступа и упростить последующий доступ
    /// к памяти, которую процесс пользователя указал ядру в системном вызове.
    pub(crate) fn check_permission<T>(
        &mut self,
        block: Block<Virt>,
        flags: PageTableFlags,
    ) -> Result<&'static [T]> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Проверяет доступность блока виртуальной памяти `block` на запись и
    /// соответствие заданным флагам доступа `flags`.
    /// Возвращает мутабельный срез типа `T`, расположенный в блоке `block`.
    ///
    /// Это позволяет объединить в одно действие проверку доступа и упростить последующий доступ
    /// к памяти, которую процесс пользователя указал ядру в системном вызове.
    pub(crate) fn check_permission_mut<T>(
        &mut self,
        block: Block<Virt>,
        flags: PageTableFlags,
    ) -> Result<&'static mut [T]> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Вспомогательный метод для
    /// [`AddressSpace::check_permission()`] и [`AddressSpace::check_permission_mut()`].
    /// Проверяет блок виртуальной памяти `block` на соответствие заданным флагам доступа `flags`.
    fn check_permission_common(
        &mut self,
        block: &Block<Virt>,
        flags: PageTableFlags,
    ) -> Result<()> {
        // TODO: your code here.
        unimplemented!();
    }
}


impl Drop for AddressSpace {
    fn drop(&mut self) {
        if Mapping::current_page_directory() == self.page_directory() {
            let base_address_space = BASE_ADDRESS_SPACE.lock();
            assert!(self.page_directory() != base_address_space.page_directory());
            base_address_space.switch_to();
            info!(address_space = %self, switch_to = %base_address_space, "drop the current address space");
        } else {
            info!(address_space = %self, "drop");
        }
    }
}


impl fmt::Display for AddressSpace {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        let page_directory = self.page_directory().address();
        match self.kind {
            Kind::Invalid => write!(formatter, "\"invalid\" @ {}", page_directory),
            Kind::Base => write!(formatter, "\"base\" @ {}", page_directory),
            Kind::Process => match self.pid {
                Some(pid) => write!(formatter, "\"{}\" @ {}", pid, page_directory),
                None => write!(formatter, "\"process\" @ {}", page_directory),
            },
        }
    }
}


/// Тип виртуального адресного пространства для логирования.
#[derive(Debug, Default)]
enum Kind {
    /// Некорректное виртуальное адресное пространство.
    #[default]
    Invalid,

    /// Базовое виртуальное адресное пространство.
    Base,

    /// Виртуальное адресное пространство некоторого процесса.
    Process,
}


lazy_static! {
    /// Базовое виртуальное адресное пространство.
    /// Создаётся загрузчиком до старта ядра и донастраивается ядром.
    /// Все остальные адресные пространства получаются из него и его копий
    /// с помощью метода [`AddressSpace::duplicate()`].
    pub static ref BASE_ADDRESS_SPACE: Mutex<AddressSpace> = Mutex::new(AddressSpace::default());
}


#[doc(hidden)]
pub(super) mod test_scaffolding {
    use crate::error::Result;

    use super::{
        super::{
            mmu::PageTableFlags,
            page_allocator::test_scaffolding::block,
            Block,
            Frame,
            Page,
            Virt,
        },
        AddressSpace,
        Mapping,
    };


    pub unsafe fn map_page(
        address_space: &mut AddressSpace,
        page: Page,
        flags: PageTableFlags,
    ) -> Result<Frame> {
        address_space.map_page(page, flags)
    }


    pub unsafe fn unmap_page(address_space: &mut AddressSpace, page: Page) -> Result<()> {
        address_space.unmap_page(page)
    }


    pub fn check_permission<T>(
        address_space: &mut AddressSpace,
        block: Block<Virt>,
        flags: PageTableFlags,
    ) -> Result<&'static [T]> {
        address_space.check_permission(block, flags)
    }


    pub fn check_permission_mut<T>(
        address_space: &mut AddressSpace,
        block: Block<Virt>,
        flags: PageTableFlags,
    ) -> Result<&'static mut [T]> {
        address_space.check_permission_mut(block, flags)
    }


    pub fn duplicate(address_space: &AddressSpace) -> Result<AddressSpace> {
        address_space.duplicate()
    }


    pub fn mapping(address_space: &mut AddressSpace) -> &mut Mapping {
        &mut address_space.mapping
    }


    pub fn page_allocator_allocate(
        address_space: &mut AddressSpace,
        size: usize,
    ) -> Result<Block<Page>> {
        address_space.page_allocator.allocate(size)
    }


    pub fn page_allocator_block(address_space: &AddressSpace) -> Block<Page> {
        block(&address_space.page_allocator)
    }
}
