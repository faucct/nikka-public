use lazy_static::lazy_static;
use x86_64::structures::tss::TaskStateSegment;

use super::stack::EXCEPTION_STACKS;


pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;
pub const PAGE_FAULT_IST_INDEX: u16 = 1;


lazy_static! {
    pub(super) static ref TSS: TaskStateSegment = {
        let mut tss = TaskStateSegment::new();

        let stacks = EXCEPTION_STACKS.lock();

        tss.interrupt_stack_table[usize::from(DOUBLE_FAULT_IST_INDEX)] =
            stacks.double_fault_rsp().into();
        tss.interrupt_stack_table[usize::from(PAGE_FAULT_IST_INDEX)] =
            stacks.page_fault_rsp().into();

        tss
    };
}
