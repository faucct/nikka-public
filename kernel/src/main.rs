#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

#![cfg_attr(feature = "deny-warnings", deny(warnings))]
#![feature(custom_test_frameworks)]
#![feature(maybe_uninit_slice)]
#![feature(panic_info_message)]
#![feature(sort_internals)]
#![no_std]
#![no_main]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]


extern crate alloc;


use core::{fmt::Write, panic::PanicInfo};

use bootloader::{entry_point, BootInfo};

use ku::time::{pit8254::Pit, rtc::Rtc};
use text::{println, Attribute};

use kernel::{
    self,
    interrupts::{INTERRUPT_STATS, RTC},
    log::debug,
    time,
    ExitCode,
    Subsystems,
};


entry_point!(kernel_main);


fn kernel_main(boot_info: &'static BootInfo) -> ! {
    kernel::init_subsystems(boot_info, Subsystems::empty());

    #[cfg(test)]
    test_main();

    while INTERRUPT_STATS[RTC].count() < 10 {
        if let Some(frequency) = Pit::tsc_per_second() {
            debug!(%frequency, "CPU frequency measured by PIT");
        }

        if let Some(frequency) = Rtc::tsc_per_second() {
            debug!(%frequency, "CPU frequency measured by RTC");
        }

        let time_precision = time::timer().lap();
        debug!(%time_precision, time_precision_in_tsc = ?time_precision);

        for (number, stats) in INTERRUPT_STATS.iter().enumerate() {
            let count = stats.count();
            if count != 0 {
                let mnemonic = stats.mnemonic();
                debug!(number, %mnemonic, count, "interrupt stats");
            }
        }

        x86_64::instructions::hlt();
    }

    kernel::exit_qemu(ExitCode::SUCCESS);
}


#[cold]
#[inline(never)]
#[no_mangle]
#[panic_handler]
fn panic(panic_info: &PanicInfo) -> ! {
    if cfg!(test) {
        kernel::fail_test(panic_info)
    } else {
        text::TEXT.lock().set_attribute(Attribute::PANIC);

        println!("{panic_info}");

        kernel::halt()
    }
}
