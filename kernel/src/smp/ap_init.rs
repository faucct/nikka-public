use core::{
    arch,
    arch::asm,
    convert::TryInto,
    fmt,
    hint,
    mem,
    sync::atomic::{AtomicBool, Ordering},
};

use chrono::Duration;
use memoffset::offset_of;
use x86::msr::IA32_EFER;
use x86_64::{
    instructions::interrupts,
    registers::{
        control::{Cr0, Cr4},
        model_specific::Efer,
    },
    structures::gdt::SegmentSelector,
};

use crate::{
    error::{Error::Timeout, Result},
    interrupts::IDT,
    log::{debug, error, info},
    memory,
    memory::{
        gdt::SmallGdt,
        size,
        Gdt,
        Page,
        Phys,
        RealModePseudoDescriptor,
        Virt,
        BASE_ADDRESS_SPACE,
        GDT,
    },
    process::Scheduler,
    time,
};

use super::{cpu::Cpu, LocalApic};


/// Запуск Application Processor из Bootstrap Processor.
///
/// MultiProcessor Specification 4.1 part B.4 "Application Processor Startup"
pub(super) fn boot_ap(phys2virt: Page, cpu: &mut Cpu) -> Result<()> {
    let timer = time::timer();
    let initialized = AtomicBool::new(false);

    let boot_code = prepare_boot_code(phys2virt, cpu, &initialized)?;

    LocalApic::send_init(cpu.id(), boot_code)?;

    let message = "boot Application Processor";

    if let Err(error) = wait_initialized(&initialized, Duration::seconds(1)) {
        error!(cpu = cpu.id(), duration = %timer.elapsed(), ?error, message);
    } else {
        info!(cpu = cpu.id(), duration = %timer.elapsed(), message);
    }

    Ok(())
}


/// Подготовка кода и стека начальной инициализации Application Processor.
fn prepare_boot_code(phys2virt: Page, cpu: &mut Cpu, initialized: &AtomicBool) -> Result<Phys> {
    let boot_code = real_mode_address(BOOT_CODE, "boot code");

    copy_switch_mode_code(memory::phys2virt_map(phys2virt, boot_code))?;

    let boot_stack_phys = real_mode_address(BOOT_STACK, "boot stack");
    let boot_stack = BootStack::new(boot_stack_phys, cpu, initialized)?;

    debug!(?boot_stack);

    unsafe {
        memory::phys2virt_map(phys2virt, boot_stack_phys)
            .try_into_mut_ptr::<BootStack>()
            .expect("BOOT_STACK address is not sutable for BootStack type")
            .write_volatile(boot_stack);
        arch::x86_64::_mm_mfence();
    }

    Ok(boot_code)
}


const BOOT_CODE: usize = 7 * Page::SIZE;
const BOOT_STACK: usize = BOOT_CODE + BOOT_CODE_PLUS_STACK_SIZE - mem::size_of::<BootStack>();
const BOOT_CODE_PLUS_STACK_SIZE: usize = Page::SIZE;


/// Стек с дополнительной информацией для загрузки AP.
///
/// Через этот стек в [`switch_from_real_mode_to_long_mode()`] передаётся дополнительная информация,
/// необходимая для загрузки AP.
/// Например, дескриптор описывающий GDT, который можно передать инструкции `lgdt` как есть,
/// или адрес корневой таблицы страниц, который можно как есть записать в `CR3`.
/// Это сделано, чтобы:
///
/// - Не нужно было в функции [`switch_from_real_mode_to_long_mode()`] на 16-битном ассемблере строить GDT,
/// таблицы страниц и т.д. То есть для максимального упрощения ассемблерной части кода.
/// - Содержимое системных регистров процессора на всех AP совпадало с аналогичными регистрами BSP.
/// То есть, чтобы можно было один раз правильно настроить системные регистры BSP,
/// и автоматически получить правильные и консистентные по системе в целом настройки всех процессоров.
///
/// Поля в [`BootStack`] организованы в удобном для [`switch_from_real_mode_to_long_mode()`] порядке.
/// В частности, поля с селектором кода [`BootStack::kernel_code`] и релоцированным адресом
/// [`BootStack::set_cs_rip_to_64bit`] метки `set_cs_rip_to_64bit:` лежат так, что образуют
/// [far pointer](https://en.wikipedia.org/wiki/Far_pointer) `kernel_code:set_cs_rip_to_64bit`,
/// который может быть использован инструкцией `far ret` как есть.
#[repr(C)]
struct BootStack {
    /// <https://wiki.osdev.org/CPU_Registers_x86-64#IA32_EFER>
    efer: u32,
    /// <https://wiki.osdev.org/CPU_Registers_x86-64#CR4>
    cr4: u32,
    /// <https://wiki.osdev.org/CPU_Registers_x86-64#CR3>
    cr3: u32,
    /// <https://wiki.osdev.org/CPU_Registers_x86-64#CR0>
    cr0: u32,
    gdt_pseudo_descriptor: RealModePseudoDescriptor,
    kernel_data: SegmentSelector,
    /// `kernel_code:set_cs_rip_to_64bit` is a 16 bit far pointer for a far return `retf` instruction.
    set_cs_rip_to_64bit: u16,
    kernel_code: SegmentSelector,
    _padding: u32,
    cpu: Virt,
    initialized: Virt,
    kernel_stack: Virt,
    small_gdt: SmallGdt,
}


impl BootStack {
    /// Подготавливает временный стек для функции [`switch_from_real_mode_to_long_mode()`].
    fn new(boot_stack_phys: Phys, cpu: &mut Cpu, initialized: &AtomicBool) -> Result<Self> {
        let switch_mode_start: usize;
        let set_cs_rip_to_64bit: usize;
        unsafe {
            asm!(
                "
                mov {switch_mode_start}, OFFSET switch_mode_start
                mov {set_cs_rip_to_64bit}, OFFSET set_cs_rip_to_64bit
                ",
                switch_mode_start = out(reg) switch_mode_start,
                set_cs_rip_to_64bit = out(reg) set_cs_rip_to_64bit,
            )
        }

        let efer = Efer::read().bits().try_into()?;
        let cr4 = Cr4::read_raw().try_into()?;
        let cr3 = BASE_ADDRESS_SPACE.lock().page_directory().address().try_into_u32()?;
        let cr0 = Cr0::read_raw().try_into()?;
        let small_gdt = SmallGdt::new();
        let gdt_pseudo_descriptor = SmallGdt::real_mode_pseudo_descriptor(
            (boot_stack_phys + offset_of!(BootStack, small_gdt))?,
        )?;
        let set_cs_rip_to_64bit: u16 = size::into_u64(BOOT_CODE + (set_cs_rip_to_64bit - switch_mode_start))
            .try_into()
            .expect("set_cs_rip_to_64bit label address should fit into 16 bit for Application Processors to be able to ret to it");

        let kernel_stack = cpu.kernel_stack().pointer();

        Ok(Self {
            efer,
            cr4,
            cr3,
            cr0,
            gdt_pseudo_descriptor,
            kernel_data: Gdt::kernel_data(),
            set_cs_rip_to_64bit,
            kernel_code: Gdt::kernel_code(),
            _padding: 0,
            cpu: Virt::from_ref(cpu),
            initialized: Virt::from_ref(initialized),
            kernel_stack,
            small_gdt,
        })
    }
}


impl fmt::Debug for BootStack {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            concat!(
                "{{ ",
                "efer: {:#X}, cr4: {:#X}, cr3: {:#X}, cr0: {:#X}, ",
                "gdt: {:?}, kernel_code: {:?}, kernel_data: {:?}, set_cs_rip_to_64bit: 0p{:04X}, ",
                "kernel_stack: {}, cpu: {}, initialized: {} ",
                "}}",
            ),
            self.efer,
            self.cr4,
            self.cr3,
            self.cr0,
            self.gdt_pseudo_descriptor,
            self.kernel_code,
            self.kernel_data,
            self.set_cs_rip_to_64bit,
            self.kernel_stack,
            self.cpu,
            self.initialized,
        )
    }
}


/// Переключает Application Processor из [реального режима](https://en.wikipedia.org/wiki/Real_mode) в [64-битный режим](https://en.wikipedia.org/wiki/Long_mode) и выполняет начальную инициализацию процессора.
///
/// Переключение режима происходит напрямую, минуя [32-х битный защищённый режим](https://en.wikipedia.org/wiki/Protected_mode).
/// Все нужные структуры и значения регистров копируются с BSP.
#[allow(named_asm_labels)]
#[cold]
#[naked]
extern "C" fn switch_from_real_mode_to_long_mode() -> ! {
    unsafe {
        asm!(
            "
            .code16

        switch_mode_start:

            // TODO: your code here.

            .code64

            // Perform a far return to `set_cs_rip_to_64bit` in order to set `CS:RIP` right.
            retf
        set_cs_rip_to_64bit:

            // TODO: your code here.

        switch_mode_end:
            ",

            // TODO: your code here.

            options(noreturn),
        );
    }
}


/// Завершает инициализацию AP.
#[cold]
#[inline(never)]
#[no_mangle]
extern "C" fn init_ap(
    cpu: &mut Cpu,            // rdi
    initialized: &AtomicBool, // rsi
) -> ! {
    GDT.lock().load();

    LocalApic::init();

    cpu.set_fs();
    cpu.set_tss();

    IDT.load();
    interrupts::enable();

    signal_initialized(initialized);
    // `initialized` is undefined beyond this point since it is on another CPU's stack.
    // And that CPU no longer waits on it.

    info!(cpu = cpu.id(), "report for duty");

    Scheduler::run();
}


/// Релоцирует код начальной загрузки Application Processor, расположенный в функции [`switch_from_real_mode_to_long_mode()`] в заданный адрес `boot_code_address`.
fn copy_switch_mode_code(boot_code_address: Virt) -> Result<()> {
    let switch_mode_code_address = Virt::from_ptr(switch_from_real_mode_to_long_mode as *const ());
    let switch_mode_code_slice =
        unsafe { switch_mode_code_address.try_into_mut_slice::<u8>(switch_mode_code_size())? };
    let boot_code_slice =
        unsafe { boot_code_address.try_into_mut_slice::<u8>(switch_mode_code_slice.len())? };

    boot_code_slice.clone_from_slice(switch_mode_code_slice);

    Ok(())
}


/// Вычисляет размер кода начальной загрузки Application Processor, расположенного в функции [`switch_from_real_mode_to_long_mode()`].
fn switch_mode_code_size() -> usize {
    let switch_mode_start: usize;
    let switch_mode_end: usize;
    unsafe {
        asm!(
            "
            mov {switch_mode_start}, OFFSET switch_mode_start
            mov {switch_mode_end}, OFFSET switch_mode_end
            ",
            switch_mode_start = out(reg) switch_mode_start,
            switch_mode_end = out(reg) switch_mode_end,
        )
    }

    let switch_mode_code_size = switch_mode_end - switch_mode_start;
    assert!(switch_mode_code_size + mem::size_of::<BootStack>() <= BOOT_CODE_PLUS_STACK_SIZE);

    switch_mode_code_size
}


/// Преобразует заданный физический адрес `address` в [`Phys`].
///
/// # Panics
///
/// Паникует, если адрес не влезает в 32 бита и потому не будет доступен в 32-битном режиме.
fn real_mode_address(address: usize, what: &'static str) -> Phys {
    let address: u32 = address
        .try_into()
        .unwrap_or_else(|_| panic!("{} address {:#X} should fit into 32 bit for Application Processors to be able to boot from it", what, address));
    Phys::new_u64(address.into()).expect("any u32 value should be a valid physical address")
}


fn signal_initialized(initialized: &AtomicBool) {
    initialized
        .compare_exchange(false, true, Ordering::AcqRel, Ordering::Relaxed)
        .expect("double or concurrent CPU initialization");
}


fn wait_initialized(initialized: &AtomicBool, timeout: Duration) -> Result<()> {
    let start = time::timer();

    while !start.has_passed(timeout) {
        if initialized.load(Ordering::Acquire) {
            return Ok(());
        }
        hint::spin_loop();
    }

    Err(Timeout)
}
