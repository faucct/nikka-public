use core::alloc::Layout;

use crate::{
    error::{Error::WrongAlignment, Result},
    log::warn,
    memory::{Block, Page, SizeOf, Virt, BASE_ADDRESS_SPACE, KERNEL_RW},
};

use super::Info;


pub(super) struct PagedAllocator {
    info: Info,
}


impl PagedAllocator {
    pub(super) const fn new(name: &'static str) -> Self {
        Self {
            info: Info::new(name),
        }
    }


    pub(super) fn name(&self) -> &str {
        self.info.name()
    }


    pub(super) fn info(&self) -> Info {
        self.info.load()
    }


    pub(super) unsafe fn allocate_pages(&self, layout: Layout) -> Result<*mut u8> {
        // TODO: your code here.
        unimplemented!();
    }


    pub(super) unsafe fn deallocate_pages(&self, ptr: *mut u8, layout: Layout) {
        // TODO: your code here.
        unimplemented!();
    }
}
