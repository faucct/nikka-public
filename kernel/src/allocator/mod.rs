mod info;
mod paged;


use core::{
    alloc::{GlobalAlloc, Layout},
    ptr,
};

use crate::log::{info, trace};

pub use info::Info;


unsafe impl GlobalAlloc for paged::PagedAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let ptr = self.allocate_pages(layout).unwrap_or(ptr::null_mut());
        trace!(?ptr, ?layout, allocator = self.name(), "alloc");
        ptr
    }


    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        trace!(?ptr, ?layout, allocator = self.name(), "dealloc");
        self.deallocate_pages(ptr, layout)
    }
}


#[global_allocator]
static ALLOCATOR: paged::PagedAllocator = paged::PagedAllocator::new("paged");


pub fn info() -> Info {
    let info = ALLOCATOR.info();

    info!(
        allocator = info.name(),
        allocations = info.allocations(),
        deallocations = info.deallocations(),
        current_allocations = info.current_allocations(),
        allocated_pages = info.allocated_pages(),
        deallocated_pages = info.deallocated_pages(),
        current_pages = info.current_pages(),
        requested_size = info.requested_size(),
        allocated_size = info.allocated_size(),
        fragmentation_loss = info.fragmentation_loss(),
        fragmentation_loss_percentage =
            format_args!("{:.3}%", info.fragmentation_loss_percentage()),
        "allocator info",
    );

    info
}
