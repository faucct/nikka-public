#![no_main]
#![no_std]
#![feature(custom_test_frameworks)]
#![reexport_test_harness_main = "test_main"]
#![test_runner(kernel::test_runner)]

extern crate alloc;
use alloc::{boxed::Box, vec::Vec};

use core::mem;

use kernel::{allocator, memory::Page, Subsystems};


mod gen_main;

gen_main!(Subsystems::MEMORY);


#[test_case]
fn basic() {
    let start_info = allocator::info();

    {
        let mut a = Box::new(1);
        *a += 1;
        assert!(*a == 2);

        let info = allocator::info();
        let requested_size = mem::size_of_val(&*a);
        assert!(info.allocations() - start_info.allocations() == 1);
        assert!(info.deallocations() - start_info.deallocations() == 0);
        assert!(info.requested_size() - start_info.requested_size() == requested_size);
        assert!(info.allocated_size() - start_info.allocated_size() >= requested_size);
        assert!(info.allocated_pages() - start_info.allocated_pages() == 1);
        assert!(info.deallocated_pages() - start_info.deallocated_pages() == 0);
    }

    let info = allocator::info();
    assert!(info.allocations() - start_info.allocations() == 1);
    assert!(info.deallocations() - start_info.deallocations() == 1);
    assert!(info.requested_size() - start_info.requested_size() == 0);
    assert!(info.allocated_size() - start_info.allocated_size() == 0);
    assert!(info.allocated_pages() - start_info.allocated_pages() == 1);
    assert!(info.deallocated_pages() - start_info.deallocated_pages() == 1);
    assert!(info.fragmentation_loss() - start_info.fragmentation_loss() == 0);
    assert!(
        info.fragmentation_loss_percentage() - start_info.fragmentation_loss_percentage() == 0.0
    );
}


#[test_case]
fn realloc() {
    let start_info = allocator::info();

    let mut vec = Vec::new();
    let mut sum = 0;

    for a in 1..3 * Page::SIZE {
        vec.push(a);
        assert!(vec.len() == a);
        sum += a;
    }

    assert!(vec.iter().sum::<usize>() == sum);

    let info = allocator::info();
    let requested_size = vec.capacity() * mem::size_of_val(&sum);
    assert!(info.requested_size() - start_info.requested_size() == requested_size);
    assert!(info.allocated_size() - start_info.allocated_size() == requested_size);
    assert!(info.fragmentation_loss() - start_info.fragmentation_loss() == 0);
    assert!(
        info.fragmentation_loss_percentage() - start_info.fragmentation_loss_percentage() <= 0.0
    );
}
