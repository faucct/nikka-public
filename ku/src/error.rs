use core::{fmt, num::TryFromIntError, result};

use super::ring_buffer;


#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Error {
    Elf(&'static str),
    FileExists,
    FileNotFound,
    Fmt(fmt::Error),
    Int(TryFromIntError),
    InvalidArgument,
    Medium,
    NoData,
    NoDisk,
    NoFrame,
    NoPage,
    NoProcess,
    NoProcessSlot,
    NotDirectory,
    Null,
    Overflow,
    PermissionDenied,
    Postcard(postcard::Error),
    RingBuffer(ring_buffer::Error),
    Timeout,
    Unimplemented,
    WrongAlignment,
}


impl From<fmt::Error> for Error {
    fn from(e: fmt::Error) -> Self {
        Error::Fmt(e)
    }
}


impl From<TryFromIntError> for Error {
    fn from(e: TryFromIntError) -> Self {
        Error::Int(e)
    }
}


impl From<postcard::Error> for Error {
    fn from(e: postcard::Error) -> Self {
        Error::Postcard(e)
    }
}


impl From<ring_buffer::Error> for Error {
    fn from(e: ring_buffer::Error) -> Self {
        Error::RingBuffer(e)
    }
}


pub type Result<T> = result::Result<T, Error>;
