#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

#![deny(warnings)]
#![feature(cell_update)]
#![feature(step_trait)]
#![no_std]
#![allow(clippy::missing_safety_doc)]


extern crate rlibc;


pub mod error;

/// Ядро предоставляет пользовательским процессам часть информации о системе,
/// сохраняя её в памяти, доступной пользователю на чтение.
/// Эта информация собирается в виде структуры общей информации о системе [`SystemInfo`]
/// и в виде структуры с информацией о текущем процессе [`ProcessInfo`].
pub mod info;

pub mod log;

/// Здесь собраны базовые примитивы для работы с памятью, которые нужны и в ядре, и в пространстве пользователя.
pub mod memory;

/// Здесь собраны функции и структуры для работы с процессами, которые нужны и в ядре, и в пространстве пользователя.
pub mod process;

pub mod ring_buffer;

/// Здесь собраны базовые примитивы для работы со временем, которые нужны и в ядре, и в пространстве пользователя.
pub mod time;


pub use error::{Error, Result};
pub use info::{
    process_info,
    set_process_info,
    set_system_info,
    system_info,
    ProcessInfo,
    SystemInfo,
};
pub use ring_buffer::{RingBuffer, RingBufferMemory, RingBufferReadTx, RingBufferWriteTx};
pub use time::{delay, now, now_ms, timer, tsc, Hz, Tsc, TscDuration};
