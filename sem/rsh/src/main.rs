#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

use std::error::Error;

use rsh;

fn main() -> Result<(), Box<dyn Error>> {
    rsh::main()
}
