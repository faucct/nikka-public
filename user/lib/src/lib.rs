#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

#![feature(asm_const)]
#![feature(naked_functions)]
#![feature(alloc_error_handler)]
#![no_std]
#![allow(clippy::missing_safety_doc)]


pub mod memory;
pub mod syscall;

extern crate alloc;

use alloc::alloc::{GlobalAlloc, Layout};

use core::panic::PanicInfo;

use tracing_core::{dispatch, dispatch::Dispatch};

use ku::{
    info,
    info::ProcessInfo,
    log::{error, LOG_COLLECTOR},
    process::ExitCode,
};


#[alloc_error_handler]
#[cold]
#[inline(never)]
fn alloc_error_handler(layout: Layout) -> ! {
    panic!("failed to allocate memory, layout = {:?}", layout)
}


struct NullAllocator;


unsafe impl GlobalAlloc for NullAllocator {
    unsafe fn alloc(&self, _layout: Layout) -> *mut u8 {
        unimplemented!();
    }

    unsafe fn dealloc(&self, _ptr: *mut u8, _layout: Layout) {
        unimplemented!();
    }
}


#[global_allocator]
static ALLOCATOR: NullAllocator = NullAllocator {};


#[no_mangle]
pub extern "C" fn _start(process_info: &'static mut ProcessInfo) -> ! {
    info::set_process_info(process_info);

    LOG_COLLECTOR.set_flush(syscall::sched_yield);

    dispatch::set_global_default(Dispatch::from_static(&LOG_COLLECTOR)).unwrap();

    extern "Rust" {
        fn main();
    }

    unsafe {
        main();
    }

    syscall::exit(ExitCode::OK.bits());
}


#[cold]
#[inline(never)]
#[no_mangle]
#[panic_handler]
fn panic(panic_info: &PanicInfo) -> ! {
    error!(message = %panic_info);
    syscall::exit(ExitCode::PANIC.bits());
}


#[macro_export]
macro_rules! entry {
    ($path:path) => {
        #[export_name = "main"]
        pub unsafe fn check_main_signature() {
            let main: fn() = $path;

            main()
        }
    };
}
